<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CityController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
Route::get('/', [HomeController::class, 'index'])->name('home');

// Authentication
Route::get('/login', [ProfileController::class, 'getLogin'])->name('get_login');
Route::post('/login', [ProfileController::class, 'postLogin'])->name('post_login');
Route::get('/logout', [ProfileController::class, 'logout'])->middleware('logged')->name('logout');
Route::get('/set_password/{user_id}', [ProfileController::class, 'getSetPassword'])->name('get_set_password');
Route::post('/set_password', [ProfileController::class, 'postSetPassword'])->name('post_set_password');

// Dashboard
Route::get('/dashboard', [ProfileController::class, 'getDashboard'])->middleware('logged')->name('dashboard');

// Resource
Route::middleware('logged')->group(function () {

    Route::get('/clients/filter_by_city', [ClientController::class, 'index'])->name('clients.filter');

    Route::resources([
        'cities'  => CityController::class,
        'clients' => ClientController::class,
        'users'   => UserController::class,
    ]);
});