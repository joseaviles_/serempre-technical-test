<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin=User::create([
            'name'         => 'Administrador',
            'email'        => 'admin@test.com',
            'password'     => Hash::make('1234-Test'),
            'set_password' => 1,
        ]);

        User::factory(9)->create();
    }
}
