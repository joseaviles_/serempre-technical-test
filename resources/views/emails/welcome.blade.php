<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
	</head>
	<body>
		
		<p>Hola {{$user->name}}.</p>

		<p>Por favor, ingresa en el siguiente enlace para establecer tu contraseña en <b>Serempre technical test</b>: <a href="{{route('get_set_password',[$user->id])}}">Clic aquí</a>.</p>

	</body>
</html>