@extends("layouts.main")

@section("titles")
    <title>Dashboard | Serempre technical test</title>
@endsection

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-tachometer-alt"></i> Dashboard</h1>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {{session('message_info')}}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="container-fluid">
            
            <div class="row mt-3">                
                <div class="col-lg-3 col-6">
                    <a href="{{route('cities.index')}}" class="small-box bg-success">
                        <div class="inner">
                            <h3>{{$count_cities}}</h3>
                            <p>Ciudades</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-map"></i>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3 col-6">
                    <a href="{{route('clients.index')}}" class="small-box bg-danger">
                        <div class="inner">
                            <h3>{{$count_clients}}</h3>
                            <p>Clientes</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-users"></i>
                        </div>
                    </a>
                </div> 
                <div class="col-lg-3 col-6">
                    <a href="{{route('users.index')}}" class="small-box bg-info">
                        <div class="inner">
                            <h3>{{$count_users}}</h3>
                            <p>Usuarios</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-user"></i>
                        </div>
                    </a>
                </div> 
            </div>

        </div>
    </section>
@endsection