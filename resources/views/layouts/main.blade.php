<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        @yield("titles")
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css?v=1')}}">
        <link rel="shortcut icon" href="{{asset('serempre.jpg')}}">
        @yield("links")
    </head>
    <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
        <div id="ajaxLoader">
            <i class="fas fa-sync fa-spin"></i>
        </div>
        <div class="wrapper">
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu"><i class="fas fa-bars"></i></a>
                    </li>
                </ul>
            </nav>
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <a href="{{route('dashboard')}}" class="brand-link">
                    <img src="{{asset('serempre.jpg')}}" alt="Serempre technical test" class="brand-image rounded">
                    <span class="brand-text font-weight-light">Serempre tech test</span>
                </a>
                <div class="sidebar">
                    @include("includes.menu_user")
                </div>
            </aside>
            <div class="content-wrapper mb-5">
                @yield("content")
            </div>
            <footer class="main-footer">
                <strong>Copyright &copy; 2021 <a class="color-master" href="{{route('dashboard')}}">Serempre technical test</a>.</strong>
                Todos los derechos reservados.
            </footer>
        </div>
        <script type="text/javascript" src="{{asset('js/app.js?v=1')}}"></script>
        <script>
            $(document).ready(function() {
                $("body").tooltip({ selector: '[data-toggle=tooltip]' });
            });
        </script>
        @yield("scripts")
    </body>
</html>