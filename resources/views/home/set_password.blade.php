@extends("layouts.home")

@section("titles")
    <title>Establecer contraseña | Serempre technical test</title>
@endsection

@section("content")
  	<div class="login-logo">
		<a href="{{route('home')}}"><img class="rounded" src="{{asset('serempre.jpg')}}" alt="Serempre technical test" width="150"></a>
  	</div>
  	<div class="card">
	    <div class="card-body login-card-body rounded">
	      	<p class="login-box-msg"><b>{{$user->name}}</b>, establece tu contraseña en <b>Serempre technical test</b></p>
	      	@if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                  	<h5><i class="icon fas fa-check"></i> Info</h5>
              		{{session('message_info')}}
                </div>
  			@endif
	      	@if($errors->any())
                <div class="alert alert-danger alert-dismissible">
              		<h5><i class="icon fas fa-ban"></i> Error</h5>
              		<ul>
			          	@foreach ($errors->all() as $error)
		                	<li>{{$error}}</li>
			          	@endforeach
		          	</ul>
	          	</div>
 	 		@endif
			<form action="{{route('post_set_password')}}" method="POST">
				@csrf
				<input name="user_id" type="hidden" value="{{$user->id}}">
		        <small class="text-muted">* Contraseña</small>
		        <div class="input-group mb-3">
		          	<input name="password" type="password" class="form-control" placeholder="Contraseña" required>
		          	<div class="input-group-append">
			            <div class="input-group-text">
			              	<span class="fas fa-lock"></span>
			            </div>
		          	</div>
		        </div>
		        <small class="text-muted">* Confirmar contraseña</small>
		        <div class="input-group mb-3">
		          	<input name="password_confirmation" type="password" class="form-control" placeholder="Contraseña" required>
		          	<div class="input-group-append">
			            <div class="input-group-text">
			              	<span class="fas fa-lock"></span>
			            </div>
		          	</div>
		        </div>
		        <div class="row">
		          	<div class="col-12">
			            <button type="submit" class="btn btn-primary btn-block">Establecer contraseña</button>
		          	</div>
		        </div>
	      	</form>
	    </div>
  	</div>
@endsection