@extends("layouts.main")

@section("titles")
    <title>Registrar usuario | Serempre technical test</title>
@endsection

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-users"></i> Usuarios</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-primary btn-sm" href="{{route('users.index')}}">Listado</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {!!session('message_info')!!}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-dark">
                <div class="card-header">
                    <h3 class="card-title">Registrar</h3>
                </div>
                <form action="{{route('users.store')}}" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">                               
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Nombre</small>
                                    <div class="input-group mb-3">
                                        <input name="name" type="text" class="form-control" placeholder="Nombre" value="{{old('name')}}" required>
                                    </div>
                                </div>                              
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Correo electrónico</small>
                                    <div class="input-group mb-3">
                                        <input name="email" type="email" class="form-control" placeholder="Correo electrónico" value="{{old('email')}}" required>
                                    </div>
                                </div>                               
                            </div>
                        </div>
                        <button type="submit" class="btn btn-dark">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection