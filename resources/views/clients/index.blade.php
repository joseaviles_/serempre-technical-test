@extends("layouts.main")

@section("titles")
    <title>Clientes | Serempre technical test</title>
@endsection

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-users"></i> Clientes</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-primary btn-sm" href="{{route('clients.create')}}">Registrar</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {!!session('message_info')!!}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!!$error!!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(count($clients)>0)
                <div class="table-responsive">
                    <table id="table-list" class="table table-bordered table-sm display table-hover table-serempre" cellspacing="0" width="100%">
                        <thead class="thead-dark">
                            <tr>
                                <th>ID</th>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Ciudad asociada</th>
                                <th>Fecha y hora de registro</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($clients as $client)
                                <tr>
                                    <td>{{$client->id}}</td>
                                    <td>{{$client->cod}}</td>
                                    <td>{{$client->name}}</td>
                                    <td>{{$client->city->name}}</td>
                                    <td>{{$client->created_at->format('d/m/Y h:i:s a')}}</td>
                                    <td>
                                        <a class="btn btn-sm btn-warning mr-1 d-inline" href="{{route('clients.edit',$client->id)}}">
                                            <i class="fa fa-edit" aria-hidden="true"></i>
                                        </a>
                                        <form class="d-inline" action="{{route('clients.destroy', [$client->id])}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-sm btn-danger">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$clients->links()}}
                </div>
            @else
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Sin registros</h5>
                    <ul>
                        <li>No existen clientes asociados a la ciudad seleccionada.</li>
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="offset-md-4 col-md-4">
                    <form id="sales" class="card card-primary" action="{{route('clients.filter')}}" method="GET">
                        <div class="card-header">
                            <h3 class="card-title">¡Consulta los clientes según las ciudades!</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body p-2">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12">              
                                        <small class="text-muted">* Ciudad</small>
                                        <div class="input-group mb-3">
                                            <select class="form-control" id="city_id" name="city_id" required>
                                                @foreach($cities as $city)
                                                    <option @if($city_id && $city_id==$city->id) selected @endif value="{{$city->id}}">{{$city->cod}} | {{$city->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-sm mt-1">Consultar</button>
                                @if($city_id)
                                    <a class="btn btn-danger btn-sm mt-1" href="{{route('clients.index')}}">Eliminar filtro</a>
                                @endif
                            </div>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            $("#city_id").select2({
                matcher: matchCustom,
                theme: "bootstrap4"
            });

            function matchCustom(params, data) {
                
                if($.trim(params.term) === '') {
                    return data;
                }

                if(typeof data.text === 'undefined') {
                    return null;
                }
                
                if(data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
                    var modifiedData = $.extend({}, data, true);
                    return modifiedData;
                }

                return null;
            }

        });
    </script>
@endsection