@extends("layouts.main")

@section("titles")
    <title>Editar cliente | Serempre technical test</title>
@endsection

@section("content")
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="nav-icon fas fa-users"></i> Clientes</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right ml-1 mt-1">
                        <li class="breadcrumb-item"><a class="btn btn-primary btn-sm" href="{{route('clients.index')}}">Listado</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            @if(session('message_info'))
                <div class="alert alert-success alert-dismissible">
                    <h5><i class="icon fas fa-check"></i> Info</h5>
                    {!!session('message_info')!!}
                </div>
            @endif
            @if($errors->any())
                <div class="alert alert-danger alert-dismissible">
                    <h5><i class="icon fas fa-ban"></i> Error</h5>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card card-dark">
                <div class="card-header">
                    <h3 class="card-title">Editar</h3>
                </div>
                <form action="{{route('clients.update',[$client->id])}}" method="POST">
                    @csrf
                    @method("PUT")
                    <input name="client_id" type="hidden" value="{{$client->id}}">
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Código</small>
                                    <div class="input-group mb-3">
                                        <input name="cod" type="text" class="form-control" placeholder="Código" value="{{$client->cod}}" required>
                                    </div>
                                </div>                                
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Nombre</small>
                                    <div class="input-group mb-3">
                                        <input name="name" type="text" class="form-control" placeholder="Nombre" value="{{$client->name}}" required>
                                    </div>
                                </div>                              
                                <div class="col-12 col-sm-3 col-md-3">              
                                    <small class="text-muted">* Ciudad</small>
                                    <div class="input-group mb-3">
                                        <select class="form-control" id="city_id" name="city_id" required>
                                            @foreach($cities as $city)
                                                <option @if($client->city_id==$city->id) selected @endif value="{{$city->id}}">{{$city->cod}} | {{$city->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-dark">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script>
        $(document).ready(function() {

            $("#city_id").select2({
                matcher: matchCustom,
                theme: "bootstrap4"
            });

            function matchCustom(params, data) {
                
                if($.trim(params.term) === '') {
                    return data;
                }

                if(typeof data.text === 'undefined') {
                    return null;
                }
                
                if(data.text.toLowerCase().indexOf(params.term.toLowerCase()) > -1) {
                    var modifiedData = $.extend({}, data, true);
                    return modifiedData;
                }

                return null;
            }

        });
    </script>
@endsection