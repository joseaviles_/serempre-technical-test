<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        
        <li class="nav-item">
            <a class="nav-link">
                <i class="nav-icon far fa-user-circle"></i>
                <p>
                    {{$user->name}}
                </p>
            </a>
        </li>
        <li class="nav-header">Principal</li>
        <li class="nav-item">
            <a href="{{route('dashboard')}}" class="nav-link {{(isset($menu_active) && $menu_active=='dashboard')?'active':''}}">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('cities.index')}}" class="nav-link {{(isset($menu_active) && $menu_active=='cities')?'active':''}}">
                <i class="nav-icon fas fa-map"></i>
                <p>
                    Ciudades
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('clients.index')}}" class="nav-link {{(isset($menu_active) && $menu_active=='clients')?'active':''}}">
                <i class="nav-icon fas fa-users"></i>
                <p>
                    Clientes
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('users.index')}}" class="nav-link {{(isset($menu_active) && $menu_active=='users')?'active':''}}">
                <i class="nav-icon fas fa-user"></i>
                <p>
                    Usuarios
                </p>
            </a>
        </li>
        <li class="nav-header">Otros</li>
        <li class="nav-item">
            <a href="{{route('logout')}}" class="nav-link">
                <i class="nav-icon fas fa-sign-in-alt"></i>
                <p>
                    Cerrar sesión
                </p>
            </a>
        </li>
    </ul>
</nav>