<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    protected $table = 'cities';

    protected $fillable = [
        'cod',
        'name',
  	];

    public function clients()
    {
        return $this->hasMany('App\Models\Client','city_id','id');
    }
}
