<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\City;
use App\Models\Client;
use App\Http\Requests\StoreClientRequest;
use App\Http\Requests\UpdateClientRequest;
use Illuminate\Support\Facades\View;

class ClientController extends Controller
{
    public function __construct() 
    {
        View::share('menu_active', 'clients');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->city_id)) {
            $clients=Client::where('city_id',$request->city_id)->paginate(10);
        }else {
            $clients=Client::paginate(10);
        }

        $cities=City::orderBy('name','asc')->get();

        return view('clients.index', ['clients'=>$clients, 'cities'=>$cities, 'city_id'=>$request->city_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities=City::orderBy('name','asc')->get();

        if(count($cities)==0)
            return redirect()->route('cities.create')->withErrors(['Debes registrar como mínimo una ciudad para proceder a registrar un cliente.']);

        return view('clients.create', ['cities'=>$cities]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClientRequest $request)
    {
        $client=Client::create([
            'cod'     => $request->cod,
            'name'    => $request->name,
            'city_id' => $request->city_id,
        ]);

        return redirect()->route('clients.index')->with(['message_info'=>'El cliente <b>'.$client->name.'</b> ha sido registrado exitosamente.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        $cities=City::orderBy('name','asc')->get();
        return view('clients.edit', ['client'=>$client, 'cities'=>$cities]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClientRequest $request, $id)
    {
        $client=Client::find($id);
        $client->update([
            'cod'     => $request->cod,
            'name'    => $request->name,
            'city_id' => $request->city_id,
        ]);

        $client=Client::find($id);
        return redirect()->route('clients.index')->with(['message_info'=>'El cliente <b>'.$client->name.'</b> ha sido actualizado exitosamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client_name=$client->name;
        $client->delete();
        return redirect()->back()->with(['message_info'=>'El cliente <b>'.$client_name.'</b> ha sido eliminado exitosamente.']);
    }
}
