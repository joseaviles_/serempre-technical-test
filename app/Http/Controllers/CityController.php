<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\City;
use App\Http\Requests\StoreCityRequest;
use App\Http\Requests\UpdateCityRequest;
use Illuminate\Support\Facades\View;

class CityController extends Controller
{
    public function __construct() 
    {
        View::share('menu_active', 'cities');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities=City::paginate(10);
        return view('cities.index', ['cities'=>$cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCityRequest $request)
    {
        $city=City::create([
            'cod'  => $request->cod,
            'name' => $request->name,
        ]);

        return redirect()->route('cities.index')->with(['message_info'=>'La ciudad <b>'.$city->name.'</b> ha sido registrada exitosamente.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        return view('cities.edit', ['city'=>$city]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCityRequest $request, $id)
    {
        $city=City::find($id);
        $city->update([
            'cod'  => $request->cod,
            'name' => $request->name,
        ]);

        $city=City::find($id);
        return redirect()->route('cities.index')->with(['message_info'=>'La ciudad <b>'.$city->name.'</b> ha sido actualizada exitosamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        $city_name=$city->name;

        if(count($city->clients)>0) {
            return redirect()->route('cities.index')->withErrors(['La ciudad <b>'.$city_name.'</b> no puede ser eliminada, ya que tiene clientes asociados.']);
        }

        $city->delete();
        return redirect()->back()->with(['message_info'=>'La ciudad <b>'.$city_name.'</b> ha sido eliminada exitosamente.']);
    }
}
