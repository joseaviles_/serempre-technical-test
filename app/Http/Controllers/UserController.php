<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{
    public function __construct() 
    {
        View::share('menu_active', 'users');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::paginate(10);
        return view('users.index', ['users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $user=User::create([
            'name'     => $request->name,
            'email'    => $request->email,
        ]);

        return redirect()->route('users.index')->with(['message_info'=>'El usuario <b>'.$user->email.'</b> ha sido registrado exitosamente.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit', ['u'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $user=User::find($id);
        $password=$request->password;
        $password_confirmation=$request->password_confirmation;

        if(isset($password)) {

            if(strlen($password)<8) {
                return redirect()->back()->withErrors(['El campo Contraseña debe contener al menos 8 caracteres.']);
            }elseif(!isset($password_confirmation)) {
                return redirect()->back()->withErrors(['El campo confirmación de Contraseña es obligatorio si deseas cambiar la contraseña.']);
            }elseif(isset($password_confirmation) && $password_confirmation!=$password) {
                return redirect()->back()->withErrors(['El campo confirmación de Contraseña no coincide.']);
            }
            
            $user->update([
                'name'     => $request->name,
                'email'    => $request->email,
                'password' => Hash::make($password),
            ]);

        }else {
            $user->update([
                'name'     => $request->name,
                'email'    => $request->email,
            ]);
        }

        $user=User::find($id);
        return redirect()->route('users.index')->with(['message_info'=>'El usuario <b>'.$user->email.'</b> ha sido actualizado exitosamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $check_user=User::getCurrent();

        if($check_user->id==$user->id)
            return redirect()->route('users.index')->withErrors(['No puedes eliminar el usuario con el que tienes la sesión activa, debes eliminar el usuario '.$user->email.' desde otro usuario/perfil diferente...']);

        $user_email=$user->email;
        $user->delete();
        return redirect()->back()->with(['message_info'=>'El usuario <b>'.$user_email.'</b> ha sido eliminado exitosamente.']);
    }
}
