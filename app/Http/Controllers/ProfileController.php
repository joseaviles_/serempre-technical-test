<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\City;
use App\Models\Client;
use App\Models\User;
use App\Http\Requests\SetPasswordRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function getLogin() 
    {
        $user=User::getCurrent();

        if($user) {
            return redirect()->route('dashboard');
        }

        return view('home.login');
    }    
    
    public function postLogin(Request $request) 
    {
        $email=$request->email;
        $password=$request->password;

        $user=User::where('email',$email)->first();

        if(Auth::attempt(['email'=>$email, 'password'=>$password], true)) {
            return redirect()->route('dashboard');
        }

        if(!$user) {
            return redirect()->back()->withInput()->withErrors('El correo electrónico que has ingresado no se encuentra registrado.');
        }elseif(!$user->set_password) {
            return redirect()->back()->withInput()->withErrors('No has establecido tu contraseña inicial, revisa tu correo electrónico y sigue las instrucciones.');
        }else {
            return redirect()->back()->withInput()->withErrors('La contraseña que has ingresado es incorrecta.');
        }
    }    
    
    public function getDashboard() 
    {
        return view('main.dashboard', ['menu_active'=>'dashboard', 'count_users'=>User::count(), 'count_cities'=>City::count(), 'count_clients'=>Client::count()]);
    }

    public function logout() 
    {
        Auth::logout();
        return redirect()->route('home');
    }

    public function getSetPassword($user_id)
    {
        Auth::logout();
        $user=User::find($user_id);

        if(!$user) {
            return redirect()->route('get_login')->withErrors(['El usuario con ID '.$user_id.' no se encuentra registrado.']);
        }

        if($user->set_password) {
            return redirect()->route('get_login')->withErrors(['El usuario con ID '.$user_id.' ya estableció su contraseña inicial.']);
        }

        return view('home.set_password', ['user'=>$user]);
    }    

    public function postSetPassword(SetPasswordRequest $request)
    {
        User::where('id',$request->user_id)->update(['password'=>Hash::make($request->password), 'set_password'=>1]);
        return redirect()->route('get_login')->with(['message_info'=>'Tu contraseña ha sido establecida exitosamente.']);
    }
}
