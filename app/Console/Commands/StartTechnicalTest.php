<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class StartTechnicalTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:start_technical_test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start technical test';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('¡Migrando la base de datos y cargando la data predefinida de testeo! Espere un momento...');
        Artisan::call('optimize');
        Artisan::call('migrate:fresh --seed');
        $this->info('USUARIO DE ACCESO');
        $this->info('Correo electrónico: admin@test.com');
        $this->info('Contraseña: 1234-Test');
        $this->info('¡Operación finalizada exitosamente! Se registraron 20 ciudades, 30 clientes y 10 usuarios. Nota: la contraseña de todos los usuarios registrados desde la factory es: 1234-Test');
    }
}
